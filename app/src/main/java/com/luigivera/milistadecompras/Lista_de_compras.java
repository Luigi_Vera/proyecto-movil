package com.luigivera.milistadecompras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Lista_de_compras extends AppCompatActivity {
    Button botonCrear, botonPedir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_compras);

        botonCrear = (Button) findViewById(R.id.btnCrear);
        botonPedir = (Button) findViewById(R.id.btnPedir);

        botonCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent miIntent = new Intent(Lista_de_compras.this, Crear_mi_lista.class);
                startActivity(miIntent);

            }
        });
        botonPedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent miIntent = new Intent(Lista_de_compras.this, Pedir_ayuda.class);
                startActivity(miIntent);

            }
        });
    }
}
